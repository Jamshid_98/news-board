package com.example.news.entity;

import com.example.news.entity.template.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by Sherlock on 01.06.2021.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class User extends AbstractEntity implements UserDetails {

 @Column(name = "last_name")
 private String lastName;

 @Column(name = "first_name")
 private String firstName;

 @Column(name = "age")
 private Integer age;

 @Column(name = "email", unique = true)
 private String email;

 @JsonIgnore
 @Column(name = "password")
 private String password;

 @ManyToMany(fetch = FetchType.LAZY)
 @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")},
         inverseJoinColumns = {@JoinColumn(name = "role_id")})
 private List<Role> roles;

 public User(Integer age, String password, String lastName, String firstName, List<Role> roles, String email) {
  this.age = age;
  this.password = password;
  this.lastName = lastName;
  this.firstName = firstName;
  this.roles = roles;
  this.email = email;
 }

 public User(String lastName, String firstName, List<Role> roles, String email) {
  this.lastName = lastName;
  this.firstName = firstName;
  this.roles = roles;
  this.email = email;
 }

 private boolean accountNonExpired = true;
 private boolean accountNonLocked = true;
 private boolean credentialsNonExpired = true;
 private boolean enabled = true;

 @Override
 public Collection<? extends GrantedAuthority> getAuthorities() {
  return this.roles;
 }

 @Override
 public String getPassword() {
  return this.password;
 }

 @Override
 public String getUsername() {
  return this.email;
 }

 @Override
 public boolean isAccountNonExpired() {
  return this.accountNonExpired;
 }

 @Override
 public boolean isAccountNonLocked() {
  return this.accountNonLocked;
 }

 @Override
 public boolean isCredentialsNonExpired() {
  return this.credentialsNonExpired;
 }

 @Override
 public boolean isEnabled() {
  return this.enabled;
 }

 @PreUpdate
 public void update() {
  setUpdatedBy(null);
 }
}
