package com.example.news.controller;

import com.example.news.entity.User;
import com.example.news.payload.ApiResponse;
import com.example.news.payload.NewsDto;
import com.example.news.security.CurrentUser;
import com.example.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/news")
public class NewsController {

    @Autowired
    NewsService newsService;


    @PostMapping("/add")
    @PreAuthorize("hasRole('ROLE_USER')")
    public HttpEntity<?> addNews(@RequestBody NewsDto dto, @CurrentUser User user){
        ApiResponse response = newsService.addNews(dto,user);
        return ResponseEntity.status(response.isSuccess()?202:409).body(response);
    }

    @GetMapping("/adminNews")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public HttpEntity<?> allFalse(){
        ApiResponse response = newsService.adminNews();
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @PutMapping("/acceptNews")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public HttpEntity<?> acceptNews(@RequestParam UUID newsId){
        ApiResponse response = newsService.acceptNews(newsId);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @GetMapping("/ownUserNews")
    @PreAuthorize("hasRole('ROLE_USER')")
    public HttpEntity<?> getUserOwnNews(@CurrentUser User user){
        ApiResponse response = newsService.getUserNews(user);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @GetMapping("/globalNews")
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public HttpEntity<?> getGlobalNews(@CurrentUser User user){
        ApiResponse response = newsService.globalAllNews(user);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }
}
