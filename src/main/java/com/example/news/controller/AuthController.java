package com.example.news.controller;

import com.example.news.entity.User;
import com.example.news.payload.ApiResponse;
import com.example.news.payload.JwtResponse;
import com.example.news.payload.ReqSignIn;
import com.example.news.payload.ReqSignUp;
import com.example.news.repository.UserRepository;
import com.example.news.security.AuthService;
import com.example.news.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AuthService authService;

    @Autowired
    UserRepository userepository;

    @PostMapping("/login")
    public HttpEntity<?> login(@Validated @RequestBody ReqSignIn reqSignIn) {
        ApiResponse response = authService.login(reqSignIn);
        if (response.isSuccess()) {
            return getApiToken(reqSignIn.getEmail(), reqSignIn.getPassword());
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
        }
    }

    @PostMapping("/register")
    public HttpEntity<?> register(@Validated @RequestBody ReqSignUp reqSignUp) {
        ApiResponse response = authService.register(reqSignUp);
        if (response.isSuccess()) {
            return getApiToken(reqSignUp.getEmail(), reqSignUp.getPassword());
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
        }
    }

    private HttpEntity<?> getApiToken(String email, String password) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password)
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        User user = userepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Email topilmadi: " + email));

        return ResponseEntity.ok(new JwtResponse(jwt));
    }
}
