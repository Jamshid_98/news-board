package com.example.news.payload;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqSignIn {
    @NotNull
    private String email;
    @NotNull
    private String password;
}
