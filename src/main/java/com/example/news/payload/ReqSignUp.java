package com.example.news.payload;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReqSignUp {
    private Integer age;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
}
