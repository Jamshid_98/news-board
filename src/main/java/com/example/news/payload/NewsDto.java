package com.example.news.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsDto {

    private String content;

    private Boolean accepted;

    private UUID userId;

    public NewsDto(String content) {
        this.content = content;
    }
}
