package com.example.news.service;

import com.example.news.entity.News;
import com.example.news.entity.User;
import com.example.news.payload.ApiResponse;
import com.example.news.payload.NewsDto;
import com.example.news.repository.NewsRepository;
import com.example.news.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class NewsService {

    @Autowired
    UserRepository userRepository;


    @Autowired
    NewsRepository newsRepository;

    public ApiResponse addNews(NewsDto newsDto, User user) {
        Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
            News news = new News(
                    newsDto.getContent(),
                    false,
                    user.getId()
            );
            newsRepository.save(news);
            return new ApiResponse(true, "saved");
        }else {
            return new ApiResponse(false,"no saved");
        }
    }

    public ApiResponse adminNews(){
        try {
            List<News> newsList = newsRepository.findByAcceptedFalse();
            return new ApiResponse(true,"success",newsList);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"error",null);
        }
    }

    public ApiResponse acceptNews(UUID newsId){
        try {
            List<News> acceptNewsList = newsRepository.updateById(newsId);
            if (acceptNewsList.isEmpty()){
                return new ApiResponse(true,"There are no news in the list",null);
            }else {
                return new ApiResponse(true,"success",acceptNewsList);
            }
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"error",null);
        }
    }

    public ApiResponse getUserNews(User user) {
        Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
        if (optionalUser.isPresent()){
            user = optionalUser.get();
            List<News> getUserList = newsRepository.findByUserId(user.getId());
            return new ApiResponse(true,"success",getUserList);
        }else {
            return new ApiResponse(false,"getUserList not found",null);
        }
    }

    public ApiResponse globalAllNews(User user){
        Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
        if (optionalUser.isPresent()){
            user=optionalUser.get();
            List<News> globalNewsList = newsRepository.findByAcceptedTrue();
            return new ApiResponse(true,"success",globalNewsList);
        }else {
            return new ApiResponse(false,"error",null);
        }
    }


}
