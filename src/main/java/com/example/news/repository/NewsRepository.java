package com.example.news.repository;

import com.example.news.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface NewsRepository extends JpaRepository<News, UUID> {

    List<News> findByAcceptedFalse();

    List<News> findByAcceptedTrue();

    @Modifying
    @Query("update News set accepted = true where id = :newsId")
    List<News> updateById(UUID newsId);

    List<News> findByUserId(UUID userId);

}
