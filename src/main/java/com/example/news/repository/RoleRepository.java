package com.example.news.repository;

import com.example.news.entity.Role;
import com.example.news.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

//@RepositoryRestResource(path = "role",collectionResourceRel = "list",excerptProjection = CustomRole.class)
public interface RoleRepository extends JpaRepository<Role, Integer> {
    List<Role> findAllByName(RoleName roleName);
    List<Role> findAllByNameIn(List<RoleName> names);
}
